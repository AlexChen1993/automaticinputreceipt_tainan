# AutomaticInputReceipt_Tainan

AutomaticInputReceipt_Tainan is a Python program for dealing semi-automatic input receipt to the web(https://tainanshopping.tw/).

1. you need to type receipt information at excel
2. Send the information to the web by this program

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install python libary.

```bash
pip install selenium
pip install xlrd==1.2.0
pip install pandas
```

## Usage

```python
# login your account
phoneNum.send_keys('phone_number')
password.send_keys('password')

# set your receipt excel path
df=pd.read_excel('D:\\python\\test.xlsx')

# set your browser path and you need to download the browser driver exe
driver = webdriver.Chrome("D:\\python\\automaticinputreceipt_tainan\\chromedriver.exe")
```


## Excel format

you can change the column name.But need to modify the corresponding code.

![image](https://gitlab.com/AlexChen1993/automaticinputreceipt_tainan/-/blob/main/example_excel.JPG)

## Demo

![image](https://gitlab.com/AlexChen1993/automaticinputreceipt_tainan/-/blob/main/demo2.gif)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)
