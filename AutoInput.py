# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time
import pandas as pd

### 以下為需要安裝的 python 套件
# pip install selenium
# pip install xlrd==1.2.0
# pip install pandas

# 設定 讀取 excel 檔路徑
df=pd.read_excel('D:\\python\\automaticinputreceipt_tainan\\receipt.xlsx')
# 設定 執行瀏覽器的執行檔位置
driver = webdriver.Chrome("D:\\python\\automaticinputreceipt_tainan\\chromedriver.exe")
driver.get('https://tainanshopping.tw/invoice')

time.sleep(1)
# 輸入帳號(手機號碼)
phoneNum=driver.find_element_by_xpath("//div[@class='login']//input[@type='text']")
phoneNum.send_keys('your_phone_number')
# 輸入密碼
password=driver.find_element_by_xpath("//div[@class='login']//input[@type='password']")
password.send_keys('password')
# 按下 登錄
button =driver.find_element_by_xpath("//div[@class='login']//button[@type='button']")
button.click()
time.sleep(1)
# 跳轉到 發票輸入頁面
driver.get('https://tainanshopping.tw/invoice')
time.sleep(3)

# 用迴圈輸入每一列發票資訊
for index, row in df.iterrows():
    # 設定 年 選擇
    yaer_select=driver.find_elements_by_xpath('//select')[0]
    Select(yaer_select).select_by_value(str(row['y']))
    # 設定 月 選擇
    month_select=driver.find_elements_by_xpath('//select')[1]
    Select(month_select).select_by_value(str(row['m']))
    # 設定 日 選擇
    day_select=driver.find_elements_by_xpath('//select')[2]
    Select(day_select).select_by_value(str(row['d']))

    # 設定 消費金額 輸入
    input=driver.find_elements_by_xpath("//div[@class='form-group']//input")[0]
    input.send_keys(str(row['t']))
    # 設定 發票號碼 輸入
    input=driver.find_elements_by_xpath("//div[@class='form-group']//input")[1]
    input.send_keys(str(row['n']))
    # 設定 賣方統編 輸入
    input=driver.find_elements_by_xpath("//div[@class='form-group']//input")[2]
    input.send_keys(str(row['s']))
    # 按下 送出鈕
    button =driver.find_element_by_xpath("//div[@class='invoice-btn-bottom']//button[@type='button']")
    button.click()
    time.sleep(0.2)
    # 按下 確認送出
    button =driver.find_element_by_xpath("//div[@class='ok']")
    button.click()
    time.sleep(2)
    # 按下 繼續輸入發票
    button =driver.find_element_by_xpath("//div[@class='invoice-block']//button")
    button.click()
    time.sleep(2)




